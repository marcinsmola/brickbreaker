package com.brickBreaker;

import java.awt.*;

/**
 * Created by marcin on 29.05.17.
 */
public class MapGenerator {
    public int map[][];
    public int brickWidith;
    public int brickHeight;
    public MapGenerator(int row, int col){
        map=new int[row][col];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                map[i][j]=1;
            }
        }
        brickWidith=540/col;
        brickHeight=150/row;
    }
    public void draw(Graphics2D g){
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j]>0){
                    g.setColor(Color.white);
                    g.fillRect(j*brickWidith+80,i*brickHeight+50,brickWidith,brickHeight);
                    g.setStroke(new BasicStroke(3));
                    g.setColor(Color.black);
                    g.drawRect(j*brickWidith+80,i*brickHeight+50,brickWidith,brickHeight);
                }
            }
        }
    }
    public void setBrickValue(int value, int row, int col){
        map[row][col]=value;

    }
}
